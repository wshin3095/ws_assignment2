/**
 * File Name: wsglobal.js
 *
 * Revision History:
 *      Wan Jae Shin, 2021-02-27 : Created
 */

// Save add page
function btnSave_click() {
    addFeedback();
}

// update edit page
function btnUpdate_click() {
    updateFeedback();
}

// Save default email in settings page
function btnSaveDefault_click() {
    saveDefaultEmail();
}

function init() {
    $("#wsBtnSave").on("click", btnSave_click);             // Save add page
    $("#wsUpdate").on("click", btnUpdate_click);            // update edit page
    $("#wsSaveDefault").on("click", btnSaveDefault_click);  // Save default email in settings page
}

$(document).ready(function(){

    init();

    // Add - checkbox is checked then 4 rating textboxes and Overall Rating will be visible.
    $("#wsChkRatings").change(function(){
        if(this.checked) {
            $("#wsAddRatings").show();
        }
        else{
            $("#wsAddRatings").hide();
        }
    });

    // Edit - checkbox is checked then 4 rating textboxes and Overall Rating will be visible.
    $("#wsEditChkRatings").change(function(){
        if(this.checked) {
            $("#wsEditRatings").show();
        }
        else{
            $("#wsEditRatings").hide();
        }
    });

    // Add - any change in the 3 rating text boxes will automatically update the overall Rating
    $("#wsAddRatings").change(function(){
        var overall = getRatings($("#wsFoodQuality").val(), $("#wsService").val(), $("#wsValue").val());
        $("#wsOverallRatings").val(overall);
    });

    // Edit - any change in the 3 rating text boxes will automatically update the overall Rating
    $("#wsEditRatings").change(function(){
        var overall = getRatings($("#wsEditFoodQuality").val(), $("#wsEditService").val(), $("#wsEditValue").val());
        $("#wsEditOverallRatings").val(overall);
    });
});