/**
 * File Name: wsfacade.js
 *
 * Revision History:
 *      Wan Jae Shin, 2021-02-27 : Created
 */

// save and validate an add feedback page
function addFeedback() {
    if (doValidate_frmAddFeedback()) {
        console.info("Add form is valid");
    }
    else {
        // if not valid show error
        console.info("Add form is INVALID");
    }
}

// Update and validate an edit feedback page
function updateFeedback() {
    if (doValidate_frmEditFeedback()) {
        console.info("Edit form is valid");
    }
    else {
        // if not valid show error
        console.info("Edit form is INVALID");
    }
}

