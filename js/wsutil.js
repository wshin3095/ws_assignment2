/**
 * File Name: wsutil.js
 *
 * Revision History:
 *      Wan Jae Shin, 2021-02-27 : Created
 */

// Calculate ratings
function getRatings(quality, service, value) {
    var overall = (Number(quality) + Number(service) + Number(value)) * 100/15;
    overall = Math.round(overall) + "%";

    return overall;
}

// Validation of add feedback page
function doValidate_frmAddFeedback() {
    // 1. fetch the ref of the form
    var frmAdd = $("#wsFrmAddFeedback");
    // 2. call validate() function on the form and pass validation rules as an object;
    frmAdd.validate({
        rules: {
            wsBusinessName:{
                required: true,
                rangelength: [2,20]
            },
            wsReviewerEmail:{
                required: true,
                email: true
            },
            wsReviewDate:{
                required: true,
                date: true
            },
            wsFoodQuality:{
                required: true,
                range: [0, 5]
            },
            wsService:{
                required: true,
                range: [0, 5]
            },
            wsValue:{
                required: true,
                range: [0, 5]
            }
        },
        messages: {
            wsBusinessName:{
                required: "Length must be 2-20 characters long",
                rangelength: "Length must be 2-20 characters long"
            },
            wsReviewerEmail:{
                required: "Please enter a valid email address",
                rangelength: "Please enter a valid email address"
            },
            wsReviewDate:{
                required: "Review date is required",
                rangelength: "Review date is required"
            },
            wsFoodQuality:{
                required: "Value must be 0-5",
                rangelength: "Value must be 0-5"
            },
            wsService:{
                required: "Value must be 0-5",
                rangelength: "Value must be 0-5"
            },
            wsValue:{
                required: "Value must be 0-5",
                rangelength: "Value must be 0-5"
            }
        }
    });

    // 3. return valid() on the form
    return frmAdd.valid();
}

// Validation of edit feedback page
function doValidate_frmEditFeedback() {
    // 1. fetch the ref of the form
    var frmEdit = $("#wsFrmEditFeedback");
    // 2. call validate() function on the form and pass validation rules as an object;
    frmEdit.validate({
        rules: {
            wsEditBusinessName:{
                required: true,
                rangelength: [2,20]
            },
            wsEditReviewerEmail:{
                required: true,
                email: true
            },
            wsEditReviewDate:{
                required: true,
                date: true
            },
            wsEditFoodQuality:{
                required: true,
                range: [0, 5]
            },
            wsEditService:{
                required: true,
                range: [0, 5]
            },
            wsEditValue:{
                required: true,
                range: [0, 5]
            }
        },
        messages: {
            wsEditBusinessName:{
                required: "Length must be 2-20 characters long",
                rangelength: "Length must be 2-20 characters long"
            },
            wsEditReviewerEmail:{
                required: "Please enter a valid email address",
                rangelength: "Please enter a valid email address"
            },
            wsEditReviewDate:{
                required: "Review date is required",
                rangelength: "Review date is required"
            },
            wsEditFoodQuality:{
                required: "Value must be 0-5",
                rangelength: "Value must be 0-5"
            },
            wsEditService:{
                required: "Value must be 0-5",
                rangelength: "Value must be 0-5"
            },
            wsEditValue:{
                required: "Value must be 0-5",
                rangelength: "Value must be 0-5"
            }
        }
    });

    // 3. return valid() on the form
    return frmEdit.valid();
}

// Save default email in settings page
function saveDefaultEmail() {
    localStorage.setItem("DefaultEmail", $("#wsDefaultReviewerEmail").val());

    if (localStorage.getItem("DefaultEmail")){
        alert("Default reviewer email saved.");
    }
    else{
        alert("Default email not found...");
    }
}

